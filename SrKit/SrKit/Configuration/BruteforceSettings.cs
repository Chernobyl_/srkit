﻿using System.Runtime.Serialization;

namespace SrKit.Configuration
{
    public enum BruteforceMode
    {
        UserPasswordDatabase,
        UserPasswordSooperate,
    }

    [DataContract]
    public sealed class BruteforceSettings
    {
        //==========================================================

        #region General settings (same for all modes)

        [DataMember(Name = "Server address", IsRequired = true)]
        public string ServerAddress
        { get; set; }

        [DataMember(Name = "Server port", IsRequired = true)]
        public int ServerPort
        { get; set; }

        [DataMember(Name = "Thread count", IsRequired = true)]
        public int ThreadCount
        { get; set; }

        [DataMember(Name = "Mode", IsRequired = true)]
        public BruteforceMode Mode
        { get; set; }

        [DataMember(Name = "Valid output path", IsRequired = true)]
        public string ValidOutputPath
        { get; set; }

        #endregion

        //==========================================================

        #region Account;Password DB mode

        [DataMember(Name = "Account;Password database path", IsRequired = true)]
        public string AccountPasswordDatabasePath
        { get; set; }

        #endregion

        //==========================================================

        #region Accounts and passwords in sooperate files mode

        [DataMember(Name = "Account list path", IsRequired = true)]
        public string AccountListPath
        { get; private set; }

        [DataMember(Name = "Password list path", IsRequired = true)]
        public string PasswordListPath
        { get; set; }

        #endregion

        //==========================================================

        public BruteforceSettings()
        {
            this.ServerAddress = "127.0.0.1";
            this.ServerPort = 15779;
            this.ThreadCount = 4;
            this.Mode = BruteforceMode.UserPasswordDatabase;
            this.ValidOutputPath = string.Empty;
            this.AccountPasswordDatabasePath = "acc_pw_db.txt";
            this.AccountListPath = "accounts.txt";
            this.PasswordListPath = "passwords.txt";
        }
    }
}
