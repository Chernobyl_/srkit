﻿using System;
using System.Windows.Forms;

namespace SrKit.Helpers
{
    public class CustomDialogs
    {
        public static string RequestTextFileSelection()
        {
            string result = string.Empty;

            var dialog = new OpenFileDialog()
            {
               Filter = "Text Documents|*.txt",
               InitialDirectory = Environment.CurrentDirectory,
            };

            var dialogResult = dialog.ShowDialog();

            if (dialogResult != DialogResult.OK)
                return result;

            result = dialog.FileName;

            return result;
        }
    }
}
