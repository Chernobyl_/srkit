﻿namespace SrKit.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbMain = new System.Windows.Forms.GroupBox();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpClientTools = new System.Windows.Forms.TabPage();
            this.tpServerTools = new System.Windows.Forms.TabPage();
            this.tpCheats = new System.Windows.Forms.TabPage();
            this.gbCheats = new System.Windows.Forms.GroupBox();
            this.tcCheats = new System.Windows.Forms.TabControl();
            this.tpCheatsBruteforce = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCheatsBruteforcePasswordsPath = new System.Windows.Forms.Button();
            this.btnCheatsBruteforceAccountsPath = new System.Windows.Forms.Button();
            this.tbCheatsBruteforcePasswordsPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCheatsBruteforceAccountsPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbCheatsBruteforceStatistics = new System.Windows.Forms.GroupBox();
            this.tbCheatsBruteforceProgress = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbCheatsBruteforceInvalidCount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbCheatsBruteforceValidCount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gbCheatsBruteforcePaths = new System.Windows.Forms.GroupBox();
            this.btnBruteforceAccountPasswordDbFile = new System.Windows.Forms.Button();
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gbCheatsBruteforceGeneral = new System.Windows.Forms.GroupBox();
            this.rbCheatsBruteforceUserPassSoopMode = new System.Windows.Forms.RadioButton();
            this.rbCheatsBruteforceUserPassDbMode = new System.Windows.Forms.RadioButton();
            this.btnCheatsBruteforceValidPath = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbCheatsBruteforceValidPath = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCheatsThreadCount = new System.Windows.Forms.NumericUpDown();
            this.tbCheatsBruteforceServerPort = new System.Windows.Forms.TextBox();
            this.tbCheatsBruteforceServerAddr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tpLog = new System.Windows.Forms.TabPage();
            this.gbLog = new System.Windows.Forms.GroupBox();
            this.lvLog = new System.Windows.Forms.ListView();
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCheatsBruteforceStartStopSwitch = new System.Windows.Forms.Button();
            this.msMain.SuspendLayout();
            this.gbMain.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tpCheats.SuspendLayout();
            this.gbCheats.SuspendLayout();
            this.tcCheats.SuspendLayout();
            this.tpCheatsBruteforce.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbCheatsBruteforceStatistics.SuspendLayout();
            this.gbCheatsBruteforcePaths.SuspendLayout();
            this.gbCheatsBruteforceGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCheatsThreadCount)).BeginInit();
            this.tpLog.SuspendLayout();
            this.gbLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.controlToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(816, 24);
            this.msMain.TabIndex = 0;
            this.msMain.Text = "menuStrip1";
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.controlToolStripMenuItem.Text = "Control";
            this.controlToolStripMenuItem.Click += new System.EventHandler(this.controlToolStripMenuItem_Click);
            // 
            // gbMain
            // 
            this.gbMain.Controls.Add(this.tcMain);
            this.gbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbMain.Location = new System.Drawing.Point(0, 24);
            this.gbMain.Name = "gbMain";
            this.gbMain.Size = new System.Drawing.Size(816, 503);
            this.gbMain.TabIndex = 1;
            this.gbMain.TabStop = false;
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpClientTools);
            this.tcMain.Controls.Add(this.tpServerTools);
            this.tcMain.Controls.Add(this.tpCheats);
            this.tcMain.Controls.Add(this.tpLog);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(3, 16);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(810, 484);
            this.tcMain.TabIndex = 0;
            // 
            // tpClientTools
            // 
            this.tpClientTools.Location = new System.Drawing.Point(4, 22);
            this.tpClientTools.Name = "tpClientTools";
            this.tpClientTools.Size = new System.Drawing.Size(802, 458);
            this.tpClientTools.TabIndex = 0;
            this.tpClientTools.Text = "Client tools";
            this.tpClientTools.UseVisualStyleBackColor = true;
            // 
            // tpServerTools
            // 
            this.tpServerTools.Location = new System.Drawing.Point(4, 22);
            this.tpServerTools.Name = "tpServerTools";
            this.tpServerTools.Size = new System.Drawing.Size(802, 458);
            this.tpServerTools.TabIndex = 1;
            this.tpServerTools.Text = "Server tools";
            this.tpServerTools.UseVisualStyleBackColor = true;
            // 
            // tpCheats
            // 
            this.tpCheats.Controls.Add(this.gbCheats);
            this.tpCheats.Location = new System.Drawing.Point(4, 22);
            this.tpCheats.Name = "tpCheats";
            this.tpCheats.Size = new System.Drawing.Size(802, 458);
            this.tpCheats.TabIndex = 3;
            this.tpCheats.Text = "Cheats";
            this.tpCheats.UseVisualStyleBackColor = true;
            // 
            // gbCheats
            // 
            this.gbCheats.Controls.Add(this.tcCheats);
            this.gbCheats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbCheats.Location = new System.Drawing.Point(0, 0);
            this.gbCheats.Name = "gbCheats";
            this.gbCheats.Size = new System.Drawing.Size(802, 458);
            this.gbCheats.TabIndex = 0;
            this.gbCheats.TabStop = false;
            // 
            // tcCheats
            // 
            this.tcCheats.Controls.Add(this.tpCheatsBruteforce);
            this.tcCheats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcCheats.Location = new System.Drawing.Point(3, 16);
            this.tcCheats.Name = "tcCheats";
            this.tcCheats.SelectedIndex = 0;
            this.tcCheats.Size = new System.Drawing.Size(796, 439);
            this.tcCheats.TabIndex = 0;
            // 
            // tpCheatsBruteforce
            // 
            this.tpCheatsBruteforce.Controls.Add(this.btnCheatsBruteforceStartStopSwitch);
            this.tpCheatsBruteforce.Controls.Add(this.groupBox1);
            this.tpCheatsBruteforce.Controls.Add(this.gbCheatsBruteforceStatistics);
            this.tpCheatsBruteforce.Controls.Add(this.gbCheatsBruteforcePaths);
            this.tpCheatsBruteforce.Controls.Add(this.gbCheatsBruteforceGeneral);
            this.tpCheatsBruteforce.Location = new System.Drawing.Point(4, 22);
            this.tpCheatsBruteforce.Name = "tpCheatsBruteforce";
            this.tpCheatsBruteforce.Size = new System.Drawing.Size(788, 413);
            this.tpCheatsBruteforce.TabIndex = 0;
            this.tpCheatsBruteforce.Text = "Bruteforce";
            this.tpCheatsBruteforce.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCheatsBruteforcePasswordsPath);
            this.groupBox1.Controls.Add(this.btnCheatsBruteforceAccountsPath);
            this.groupBox1.Controls.Add(this.tbCheatsBruteforcePasswordsPath);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbCheatsBruteforceAccountsPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(3, 269);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 83);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Paths (sooperate acc and pass files)";
            // 
            // btnCheatsBruteforcePasswordsPath
            // 
            this.btnCheatsBruteforcePasswordsPath.Location = new System.Drawing.Point(364, 43);
            this.btnCheatsBruteforcePasswordsPath.Name = "btnCheatsBruteforcePasswordsPath";
            this.btnCheatsBruteforcePasswordsPath.Size = new System.Drawing.Size(34, 23);
            this.btnCheatsBruteforcePasswordsPath.TabIndex = 10;
            this.btnCheatsBruteforcePasswordsPath.Text = "...";
            this.btnCheatsBruteforcePasswordsPath.UseVisualStyleBackColor = true;
            this.btnCheatsBruteforcePasswordsPath.Click += new System.EventHandler(this.btnCheatsBruteforcePasswordsPath_Click);
            // 
            // btnCheatsBruteforceAccountsPath
            // 
            this.btnCheatsBruteforceAccountsPath.Location = new System.Drawing.Point(364, 18);
            this.btnCheatsBruteforceAccountsPath.Name = "btnCheatsBruteforceAccountsPath";
            this.btnCheatsBruteforceAccountsPath.Size = new System.Drawing.Size(34, 23);
            this.btnCheatsBruteforceAccountsPath.TabIndex = 9;
            this.btnCheatsBruteforceAccountsPath.Text = "...";
            this.btnCheatsBruteforceAccountsPath.UseVisualStyleBackColor = true;
            this.btnCheatsBruteforceAccountsPath.Click += new System.EventHandler(this.btnCheatsBruteforceAccountsPath_Click);
            // 
            // tbCheatsBruteforcePasswordsPath
            // 
            this.tbCheatsBruteforcePasswordsPath.Location = new System.Drawing.Point(81, 45);
            this.tbCheatsBruteforcePasswordsPath.Name = "tbCheatsBruteforcePasswordsPath";
            this.tbCheatsBruteforcePasswordsPath.ReadOnly = true;
            this.tbCheatsBruteforcePasswordsPath.Size = new System.Drawing.Size(277, 20);
            this.tbCheatsBruteforcePasswordsPath.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Passwords";
            // 
            // tbCheatsBruteforceAccountsPath
            // 
            this.tbCheatsBruteforceAccountsPath.Location = new System.Drawing.Point(81, 20);
            this.tbCheatsBruteforceAccountsPath.Name = "tbCheatsBruteforceAccountsPath";
            this.tbCheatsBruteforceAccountsPath.ReadOnly = true;
            this.tbCheatsBruteforceAccountsPath.Size = new System.Drawing.Size(277, 20);
            this.tbCheatsBruteforceAccountsPath.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Accounts";
            // 
            // gbCheatsBruteforceStatistics
            // 
            this.gbCheatsBruteforceStatistics.Controls.Add(this.tbCheatsBruteforceProgress);
            this.gbCheatsBruteforceStatistics.Controls.Add(this.label12);
            this.gbCheatsBruteforceStatistics.Controls.Add(this.tbCheatsBruteforceInvalidCount);
            this.gbCheatsBruteforceStatistics.Controls.Add(this.label10);
            this.gbCheatsBruteforceStatistics.Controls.Add(this.tbCheatsBruteforceValidCount);
            this.gbCheatsBruteforceStatistics.Controls.Add(this.label9);
            this.gbCheatsBruteforceStatistics.Location = new System.Drawing.Point(419, 3);
            this.gbCheatsBruteforceStatistics.Name = "gbCheatsBruteforceStatistics";
            this.gbCheatsBruteforceStatistics.Size = new System.Drawing.Size(351, 151);
            this.gbCheatsBruteforceStatistics.TabIndex = 7;
            this.gbCheatsBruteforceStatistics.TabStop = false;
            this.gbCheatsBruteforceStatistics.Text = "Statistics";
            // 
            // tbCheatsBruteforceProgress
            // 
            this.tbCheatsBruteforceProgress.Location = new System.Drawing.Point(113, 61);
            this.tbCheatsBruteforceProgress.Name = "tbCheatsBruteforceProgress";
            this.tbCheatsBruteforceProgress.ReadOnly = true;
            this.tbCheatsBruteforceProgress.Size = new System.Drawing.Size(55, 20);
            this.tbCheatsBruteforceProgress.TabIndex = 18;
            this.tbCheatsBruteforceProgress.Text = "0/0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Progress";
            // 
            // tbCheatsBruteforceInvalidCount
            // 
            this.tbCheatsBruteforceInvalidCount.Location = new System.Drawing.Point(113, 37);
            this.tbCheatsBruteforceInvalidCount.Name = "tbCheatsBruteforceInvalidCount";
            this.tbCheatsBruteforceInvalidCount.ReadOnly = true;
            this.tbCheatsBruteforceInvalidCount.Size = new System.Drawing.Size(55, 20);
            this.tbCheatsBruteforceInvalidCount.TabIndex = 16;
            this.tbCheatsBruteforceInvalidCount.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Invalid";
            // 
            // tbCheatsBruteforceValidCount
            // 
            this.tbCheatsBruteforceValidCount.Location = new System.Drawing.Point(113, 13);
            this.tbCheatsBruteforceValidCount.Name = "tbCheatsBruteforceValidCount";
            this.tbCheatsBruteforceValidCount.ReadOnly = true;
            this.tbCheatsBruteforceValidCount.Size = new System.Drawing.Size(55, 20);
            this.tbCheatsBruteforceValidCount.TabIndex = 14;
            this.tbCheatsBruteforceValidCount.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Valid";
            // 
            // gbCheatsBruteforcePaths
            // 
            this.gbCheatsBruteforcePaths.Controls.Add(this.btnBruteforceAccountPasswordDbFile);
            this.gbCheatsBruteforcePaths.Controls.Add(this.tbCheatsBruteforceAccountPasswordDbInputFilePath);
            this.gbCheatsBruteforcePaths.Controls.Add(this.label6);
            this.gbCheatsBruteforcePaths.Location = new System.Drawing.Point(3, 206);
            this.gbCheatsBruteforcePaths.Name = "gbCheatsBruteforcePaths";
            this.gbCheatsBruteforcePaths.Size = new System.Drawing.Size(410, 57);
            this.gbCheatsBruteforcePaths.TabIndex = 6;
            this.gbCheatsBruteforcePaths.TabStop = false;
            this.gbCheatsBruteforcePaths.Text = "Paths (account;password)";
            // 
            // btnBruteforceAccountPasswordDbFile
            // 
            this.btnBruteforceAccountPasswordDbFile.Location = new System.Drawing.Point(364, 20);
            this.btnBruteforceAccountPasswordDbFile.Name = "btnBruteforceAccountPasswordDbFile";
            this.btnBruteforceAccountPasswordDbFile.Size = new System.Drawing.Size(34, 23);
            this.btnBruteforceAccountPasswordDbFile.TabIndex = 8;
            this.btnBruteforceAccountPasswordDbFile.Text = "...";
            this.btnBruteforceAccountPasswordDbFile.UseVisualStyleBackColor = true;
            this.btnBruteforceAccountPasswordDbFile.Click += new System.EventHandler(this.btnBruteforceAccountPasswordDbFile_Click);
            // 
            // tbCheatsBruteforceAccountPasswordDbInputFilePath
            // 
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath.Location = new System.Drawing.Point(81, 20);
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath.Name = "tbCheatsBruteforceAccountPasswordDbInputFilePath";
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath.ReadOnly = true;
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath.Size = new System.Drawing.Size(277, 20);
            this.tbCheatsBruteforceAccountPasswordDbInputFilePath.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Input file";
            // 
            // gbCheatsBruteforceGeneral
            // 
            this.gbCheatsBruteforceGeneral.Controls.Add(this.rbCheatsBruteforceUserPassSoopMode);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.rbCheatsBruteforceUserPassDbMode);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.btnCheatsBruteforceValidPath);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.label7);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.tbCheatsBruteforceValidPath);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.label8);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.tbCheatsThreadCount);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.tbCheatsBruteforceServerPort);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.tbCheatsBruteforceServerAddr);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.label3);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.label2);
            this.gbCheatsBruteforceGeneral.Controls.Add(this.label1);
            this.gbCheatsBruteforceGeneral.Location = new System.Drawing.Point(3, 3);
            this.gbCheatsBruteforceGeneral.Name = "gbCheatsBruteforceGeneral";
            this.gbCheatsBruteforceGeneral.Size = new System.Drawing.Size(410, 151);
            this.gbCheatsBruteforceGeneral.TabIndex = 0;
            this.gbCheatsBruteforceGeneral.TabStop = false;
            this.gbCheatsBruteforceGeneral.Text = "General";
            // 
            // rbCheatsBruteforceUserPassSoopMode
            // 
            this.rbCheatsBruteforceUserPassSoopMode.AutoSize = true;
            this.rbCheatsBruteforceUserPassSoopMode.Location = new System.Drawing.Point(175, 88);
            this.rbCheatsBruteforceUserPassSoopMode.Name = "rbCheatsBruteforceUserPassSoopMode";
            this.rbCheatsBruteforceUserPassSoopMode.Size = new System.Drawing.Size(130, 17);
            this.rbCheatsBruteforceUserPassSoopMode.TabIndex = 13;
            this.rbCheatsBruteforceUserPassSoopMode.TabStop = true;
            this.rbCheatsBruteforceUserPassSoopMode.Text = "User / pass sooperate";
            this.rbCheatsBruteforceUserPassSoopMode.UseVisualStyleBackColor = true;
            // 
            // rbCheatsBruteforceUserPassDbMode
            // 
            this.rbCheatsBruteforceUserPassDbMode.AutoSize = true;
            this.rbCheatsBruteforceUserPassDbMode.Location = new System.Drawing.Point(58, 88);
            this.rbCheatsBruteforceUserPassDbMode.Name = "rbCheatsBruteforceUserPassDbMode";
            this.rbCheatsBruteforceUserPassDbMode.Size = new System.Drawing.Size(111, 17);
            this.rbCheatsBruteforceUserPassDbMode.TabIndex = 12;
            this.rbCheatsBruteforceUserPassDbMode.TabStop = true;
            this.rbCheatsBruteforceUserPassDbMode.Text = "user;password DB";
            this.rbCheatsBruteforceUserPassDbMode.UseVisualStyleBackColor = true;
            // 
            // btnCheatsBruteforceValidPath
            // 
            this.btnCheatsBruteforceValidPath.Location = new System.Drawing.Point(364, 115);
            this.btnCheatsBruteforceValidPath.Name = "btnCheatsBruteforceValidPath";
            this.btnCheatsBruteforceValidPath.Size = new System.Drawing.Size(34, 23);
            this.btnCheatsBruteforceValidPath.TabIndex = 11;
            this.btnCheatsBruteforceValidPath.Text = "...";
            this.btnCheatsBruteforceValidPath.UseVisualStyleBackColor = true;
            this.btnCheatsBruteforceValidPath.Click += new System.EventHandler(this.btnCheatsBruteforceValidPath_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Mode";
            // 
            // tbCheatsBruteforceValidPath
            // 
            this.tbCheatsBruteforceValidPath.Location = new System.Drawing.Point(58, 117);
            this.tbCheatsBruteforceValidPath.Name = "tbCheatsBruteforceValidPath";
            this.tbCheatsBruteforceValidPath.ReadOnly = true;
            this.tbCheatsBruteforceValidPath.Size = new System.Drawing.Size(300, 20);
            this.tbCheatsBruteforceValidPath.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Valids";
            // 
            // tbCheatsThreadCount
            // 
            this.tbCheatsThreadCount.Location = new System.Drawing.Point(58, 65);
            this.tbCheatsThreadCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbCheatsThreadCount.Name = "tbCheatsThreadCount";
            this.tbCheatsThreadCount.Size = new System.Drawing.Size(50, 20);
            this.tbCheatsThreadCount.TabIndex = 5;
            this.tbCheatsThreadCount.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // tbCheatsBruteforceServerPort
            // 
            this.tbCheatsBruteforceServerPort.Location = new System.Drawing.Point(58, 41);
            this.tbCheatsBruteforceServerPort.Name = "tbCheatsBruteforceServerPort";
            this.tbCheatsBruteforceServerPort.Size = new System.Drawing.Size(50, 20);
            this.tbCheatsBruteforceServerPort.TabIndex = 4;
            this.tbCheatsBruteforceServerPort.Text = "15779";
            // 
            // tbCheatsBruteforceServerAddr
            // 
            this.tbCheatsBruteforceServerAddr.Location = new System.Drawing.Point(58, 17);
            this.tbCheatsBruteforceServerAddr.Name = "tbCheatsBruteforceServerAddr";
            this.tbCheatsBruteforceServerAddr.Size = new System.Drawing.Size(100, 20);
            this.tbCheatsBruteforceServerAddr.TabIndex = 3;
            this.tbCheatsBruteforceServerAddr.Text = "127.0.0.1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Threads";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Address";
            // 
            // tpLog
            // 
            this.tpLog.Controls.Add(this.gbLog);
            this.tpLog.Location = new System.Drawing.Point(4, 22);
            this.tpLog.Name = "tpLog";
            this.tpLog.Size = new System.Drawing.Size(802, 458);
            this.tpLog.TabIndex = 2;
            this.tpLog.Text = "Log";
            this.tpLog.UseVisualStyleBackColor = true;
            // 
            // gbLog
            // 
            this.gbLog.Controls.Add(this.lvLog);
            this.gbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbLog.Location = new System.Drawing.Point(0, 0);
            this.gbLog.Name = "gbLog";
            this.gbLog.Size = new System.Drawing.Size(802, 458);
            this.gbLog.TabIndex = 0;
            this.gbLog.TabStop = false;
            // 
            // lvLog
            // 
            this.lvLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTime,
            this.colLevel,
            this.colMessage});
            this.lvLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvLog.FullRowSelect = true;
            this.lvLog.GridLines = true;
            this.lvLog.Location = new System.Drawing.Point(3, 16);
            this.lvLog.MultiSelect = false;
            this.lvLog.Name = "lvLog";
            this.lvLog.Size = new System.Drawing.Size(796, 439);
            this.lvLog.TabIndex = 0;
            this.lvLog.UseCompatibleStateImageBehavior = false;
            this.lvLog.View = System.Windows.Forms.View.Details;
            // 
            // colTime
            // 
            this.colTime.Text = "Time";
            this.colTime.Width = 130;
            // 
            // colLevel
            // 
            this.colLevel.Text = "Level";
            this.colLevel.Width = 80;
            // 
            // colMessage
            // 
            this.colMessage.Text = "Message";
            this.colMessage.Width = 450;
            // 
            // btnCheatsBruteforceStartStopSwitch
            // 
            this.btnCheatsBruteforceStartStopSwitch.Location = new System.Drawing.Point(428, 206);
            this.btnCheatsBruteforceStartStopSwitch.Name = "btnCheatsBruteforceStartStopSwitch";
            this.btnCheatsBruteforceStartStopSwitch.Size = new System.Drawing.Size(342, 57);
            this.btnCheatsBruteforceStartStopSwitch.TabIndex = 8;
            this.btnCheatsBruteforceStartStopSwitch.Text = "Start";
            this.btnCheatsBruteforceStartStopSwitch.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 527);
            this.Controls.Add(this.gbMain);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "MainForm";
            this.Text = "SrKit";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.gbMain.ResumeLayout(false);
            this.tcMain.ResumeLayout(false);
            this.tpCheats.ResumeLayout(false);
            this.gbCheats.ResumeLayout(false);
            this.tcCheats.ResumeLayout(false);
            this.tpCheatsBruteforce.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbCheatsBruteforceStatistics.ResumeLayout(false);
            this.gbCheatsBruteforceStatistics.PerformLayout();
            this.gbCheatsBruteforcePaths.ResumeLayout(false);
            this.gbCheatsBruteforcePaths.PerformLayout();
            this.gbCheatsBruteforceGeneral.ResumeLayout(false);
            this.gbCheatsBruteforceGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCheatsThreadCount)).EndInit();
            this.tpLog.ResumeLayout(false);
            this.gbLog.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbMain;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpClientTools;
        private System.Windows.Forms.TabPage tpServerTools;
        private System.Windows.Forms.TabPage tpCheats;
        private System.Windows.Forms.TabPage tpLog;
        private System.Windows.Forms.GroupBox gbLog;
        private System.Windows.Forms.ListView lvLog;
        private System.Windows.Forms.ColumnHeader colTime;
        private System.Windows.Forms.ColumnHeader colLevel;
        private System.Windows.Forms.ColumnHeader colMessage;
        private System.Windows.Forms.GroupBox gbCheats;
        private System.Windows.Forms.TabControl tcCheats;
        private System.Windows.Forms.TabPage tpCheatsBruteforce;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCheatsBruteforcePasswordsPath;
        private System.Windows.Forms.Button btnCheatsBruteforceAccountsPath;
        private System.Windows.Forms.TextBox tbCheatsBruteforcePasswordsPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbCheatsBruteforceAccountsPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbCheatsBruteforceStatistics;
        private System.Windows.Forms.GroupBox gbCheatsBruteforcePaths;
        private System.Windows.Forms.Button btnBruteforceAccountPasswordDbFile;
        private System.Windows.Forms.TextBox tbCheatsBruteforceAccountPasswordDbInputFilePath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gbCheatsBruteforceGeneral;
        private System.Windows.Forms.Button btnCheatsBruteforceValidPath;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbCheatsBruteforceValidPath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown tbCheatsThreadCount;
        private System.Windows.Forms.TextBox tbCheatsBruteforceServerPort;
        private System.Windows.Forms.TextBox tbCheatsBruteforceServerAddr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbCheatsBruteforceUserPassSoopMode;
        private System.Windows.Forms.RadioButton rbCheatsBruteforceUserPassDbMode;
        private System.Windows.Forms.TextBox tbCheatsBruteforceProgress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbCheatsBruteforceInvalidCount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbCheatsBruteforceValidCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCheatsBruteforceStartStopSwitch;
    }
}

