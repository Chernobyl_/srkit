﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace SrKit.Configuration
{
    [DataContract]
    public sealed class Settings
    {
        [DataMember(Name = "Bruteforce config", IsRequired = true)]
        public BruteforceSettings BruteforceConfig
        { get; private set; }


        public Settings()
        {
        }

        public static Settings GenerateDefaults()
        {
            var inst = new Settings()
            {
                BruteforceConfig = new BruteforceSettings()
            };
            return inst;
        }

    }
}
