﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SrKit.Helpers;
using SrKit.Managers;
using System.Threading;


namespace SrKit.Forms
{
    public partial class MainForm : Form
    {
       
        public MainForm()
        {
            InitializeComponent();
        }

        private void SetupBruteforceControlsFromSettings()
        {
            var bruteCfg = gs.Settings.BruteforceConfig;
            tbCheatsBruteforceServerAddr.Text = bruteCfg.ServerAddress;
            tbCheatsBruteforceServerPort.Text = bruteCfg.ServerPort.ToString();
            tbCheatsThreadCount.Value = bruteCfg.ThreadCount;
            if (bruteCfg.Mode == Configuration.BruteforceMode.UserPasswordDatabase)
                rbCheatsBruteforceUserPassDbMode.Checked = true;
            else rbCheatsBruteforceUserPassSoopMode.Checked = true;

            tbCheatsBruteforceValidPath.Text = bruteCfg.ValidOutputPath;
            tbCheatsBruteforceAccountPasswordDbInputFilePath.Text = bruteCfg.AccountPasswordDatabasePath;
            tbCheatsBruteforceAccountsPath.Text = bruteCfg.AccountListPath;
            tbCheatsBruteforcePasswordsPath.Text = bruteCfg.PasswordListPath;
        }

        void Test()
        {

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    SetupBruteforceControlsFromSettings();
                }));
            }
            else
            {
                SetupBruteforceControlsFromSettings();
            }
        }

        public void InitializeFromSettings()
        {
            new Thread(Test).Start();

        }

        private void controlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gs.LogManager.WriteToView(LogLevel.Normal, "Testing {0}", "arguments !");
            gs.LogManager.WriteToView(LogLevel.Warning, "WAaaaarning !");
            gs.LogManager.WriteToView(LogLevel.Error, "FUCK OMG ERROR !");
            gs.LogManager.WriteToView(LogLevel.Fatal, "Even worse man go kyss !");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            gs.AssignMainForm(this);
            gs.LogManager.AssignLogListView(lvLog);
            gs.LoadSettings();
        }


        private void btnCheatsBruteforceValidPath_Click(object sender, EventArgs e)
        {
            string filePath = CustomDialogs.RequestTextFileSelection();
            if (filePath == string.Empty)
            {
                gs.LogManager.WriteToView(LogLevel.Warning,
                    "[Bruteforce] Invalid path for valid output");
                return;
            }
            gs.LogManager.WriteToView(LogLevel.Normal,
                $"[Bruteforce] Selected path for valid output: \"{filePath}\"");

            if(tbCheatsBruteforceValidPath.InvokeRequired)
            {
                tbCheatsBruteforceValidPath.Invoke(new MethodInvoker(delegate ()
                {
                    tbCheatsBruteforceValidPath.Text = filePath;
                }));
            }
            else
            {
                tbCheatsBruteforceValidPath.Text = filePath;
            }
        }

        private void btnBruteforceAccountPasswordDbFile_Click(object sender, EventArgs e)
        {
            string filePath = CustomDialogs.RequestTextFileSelection();
            if (filePath == string.Empty)
            {
                gs.LogManager.WriteToView(LogLevel.Warning,
                    "[Bruteforce] Invalid path for account/password database");
                return;
            }

            gs.LogManager.WriteToView(LogLevel.Normal,
               $"[Bruteforce] Selected path for account/password database: \"{filePath}\"");


            if (tbCheatsBruteforceAccountPasswordDbInputFilePath.InvokeRequired)
            {
                tbCheatsBruteforceAccountPasswordDbInputFilePath.Invoke(new MethodInvoker(delegate ()
                {
                    tbCheatsBruteforceAccountPasswordDbInputFilePath.Text = filePath;
                }));
            }
            else
            {
                tbCheatsBruteforceAccountPasswordDbInputFilePath.Text = filePath;
            }

        }

        private void btnCheatsBruteforceAccountsPath_Click(object sender, EventArgs e)
        {
            string filePath = CustomDialogs.RequestTextFileSelection();
            if (filePath == string.Empty)
            {
                gs.LogManager.WriteToView(LogLevel.Warning,
                    "[Bruteforce] Invalid path for account list");
                return;
            }

            gs.LogManager.WriteToView(LogLevel.Normal,
               $"[Bruteforce] Selected path for account list: \"{filePath}\"");


            if (tbCheatsBruteforceAccountsPath.InvokeRequired)
            {
                tbCheatsBruteforceAccountsPath.Invoke(new MethodInvoker(delegate ()
                {
                    tbCheatsBruteforceAccountsPath.Text = filePath;
                }));
            }
            else
            {
                tbCheatsBruteforceAccountsPath.Text = filePath;
            }

        }

        private void btnCheatsBruteforcePasswordsPath_Click(object sender, EventArgs e)
        {
            string filePath = CustomDialogs.RequestTextFileSelection();
            if (filePath == string.Empty)
            {
                gs.LogManager.WriteToView(LogLevel.Warning,
                    "[Bruteforce] Invalid path for password list");
                return;
            }

            gs.LogManager.WriteToView(LogLevel.Normal,
               $"[Bruteforce] Selected path for password list: \"{filePath}\"");


            if (tbCheatsBruteforcePasswordsPath.InvokeRequired)
            {
                tbCheatsBruteforcePasswordsPath.Invoke(new MethodInvoker(delegate ()
                {
                    tbCheatsBruteforcePasswordsPath.Text = filePath;
                }));
            }
            else
            {
                tbCheatsBruteforcePasswordsPath.Text = filePath;
            }

        }
    }
}
