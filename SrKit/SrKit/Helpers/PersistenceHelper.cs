﻿using Newtonsoft.Json;
using System.IO;


namespace SrKit.Helpers
{

    /// <summary>
    /// The persistance helper class. This will include configuration reading layer (in case
    /// we want to use and T-SQL database in future put interaction wrapper here).
    /// </summary>
    public static class PersistenceHelper
    {
        //--------------------------------------------------------------------------

        #region Public methods


        /// <summary>
        /// Converts object of given type to json string.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>The json-serialized, indented string.</returns>
        public static string ToJson<T>(T obj) =>
            JsonConvert.SerializeObject(obj, Formatting.Indented);

        /// <summary>
        /// Converts json string to object of given type.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="str">The json string to deserialize.</param>
        /// <returns></returns>
        public static T FromJson<T>(string str) =>
            JsonConvert.DeserializeObject<T>(str);

        /// <summary>
        /// Reads object of given type from file.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="path">The path.</param>
        /// <returns>
        /// Object instance, or default(T). 
        /// This will obviosly be null for nullable types
        /// </returns>
        public static T ReadFile<T>(string path)
        {
            T obj = default(T);
            try
            {
                string str = File.ReadAllText(path);
                return FromJson<T>(str);
            }
            catch { }
            return obj;
        }

        /// <summary>
        /// Writes object of given type to file.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="obj">The object.</param>
        /// <param name="path">The path.</param>
        /// <returns>true / false</returns>
        public static bool WriteFile<T>(T obj, string path)
        {
            try
            {
                string str = ToJson<T>(obj);
                File.WriteAllText(path, str);
                return true;
            }
            catch { }
            return false;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
