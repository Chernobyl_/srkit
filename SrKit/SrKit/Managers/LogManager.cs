﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;

namespace SrKit.Managers
{
    public enum LogLevel
    {
        Normal,
        Warning,
        Error,
        Fatal
    }
    public sealed class LogManager
    {
        private ListView _logView;

        public void AssignLogListView(ListView logListView)
        {
            _logView = logListView;
        }

        /// <summary>
        /// TODO: Better coloring set from a control (see palette chooser...)
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private static Color GetRowBackgroundColorByLogLevel(LogLevel level)
        {
            var result = Color.White;
            switch(level)
            {
                case LogLevel.Normal:
                    {
                        result = Color.DarkCyan;
                    }
                    break;
                case LogLevel.Warning:
                    {
                        result = Color.Cyan;
                    }
                    break;

                case LogLevel.Error:
                    {
                        result = Color.MediumPurple;
                    }
                    break;
                case LogLevel.Fatal:
                    {
                        result = Color.Red;
                    }
                    break;
            }

            return result;
        }

        public void WriteToView(LogLevel level, string msg, params object[] args)
        {
            if (_logView == null)
                throw new InvalidOperationException("Log list view must be assigned before usage.");

            var lvi = new ListViewItem(DateTime.Now.ToString());
            lvi.SubItems.Add(level.ToString());

            string finalMsg = string.Empty;
            try
            {
                finalMsg = string.Format(msg, args);
            }
            catch { finalMsg = $"Invalid format:  {msg}"; }

            lvi.SubItems.Add(finalMsg);


            if (_logView.InvokeRequired)
            {
                _logView.Invoke(new MethodInvoker(delegate ()
                {
                    _logView.Items.Add(lvi);
                }));
            }
            else _logView.Items.Add(lvi);


            //And finally, highlight our log levels :)
            lvi.BackColor = GetRowBackgroundColorByLogLevel(level);
       

            _logView.EnsureVisible(_logView.Items.Count - 1);
        }
    }
}