﻿using SrKit.Configuration;
using SrKit.Helpers;
using SrKit.Managers;
using System.IO;
using SrKit.Forms;

namespace SrKit
{
    class gs
    {
        public static LogManager LogManager = new LogManager();
        public static Settings Settings;
        private static MainForm MainForm;

        private const string SettingsPath = "settings.json";

        public static void AssignMainForm(MainForm form) => gs.MainForm = form;

        public static bool LoadSettings()
        {
            Settings settings = null;

            if(!File.Exists("settings.json"))
            {
                settings = Settings.GenerateDefaults();
                PersistenceHelper.WriteFile<Settings>(settings, SettingsPath);
                gs.LogManager.WriteToView(LogLevel.Normal, "Default settings generated");
            }

            gs.Settings = PersistenceHelper.ReadFile<Settings>(SettingsPath);
            if (gs.Settings == null)
                return false;

            gs.MainForm.InitializeFromSettings();

            return true;
        }
    }
}
